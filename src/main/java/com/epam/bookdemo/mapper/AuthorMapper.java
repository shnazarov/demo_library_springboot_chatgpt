package com.epam.bookdemo.mapper;


import com.epam.bookdemo.dto.AuthorDTO;
import com.epam.bookdemo.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);

    AuthorDTO toDTO(Author model);

    Author toModel(AuthorDTO dto);

    List<Author> toModels(List<AuthorDTO> dtoList);

    List<AuthorDTO> toDTOs(List<Author> models);
}
