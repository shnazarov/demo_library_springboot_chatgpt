package com.epam.bookdemo.mapper;


import com.epam.bookdemo.dto.BookDTO;
import com.epam.bookdemo.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BookMapper {
    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    BookDTO toDTO(Book model);

    Book toModel(BookDTO dto);

    List<Book> toModels(List<BookDTO> dtoList);

    List<BookDTO> toDTOs(List<Book> models);

}
