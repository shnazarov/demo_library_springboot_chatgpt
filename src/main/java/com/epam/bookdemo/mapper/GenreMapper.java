package com.epam.bookdemo.mapper;

import com.epam.bookdemo.dto.GenreDTO;
import com.epam.bookdemo.model.Genre;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface GenreMapper {
    GenreMapper INSTANCE = Mappers.getMapper(GenreMapper.class);

    GenreDTO toDTO(Genre model);

    Genre toModel(GenreDTO dto);

    List<Genre> toModels(List<GenreDTO> dtoList);

    List<GenreDTO> toDTOs(List<Genre> models);
}
