package com.epam.bookdemo.dto;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BookDTO {
    private Long id;
    private String title;
    private double price;
    private int quantity;

    private AuthorDTO author;

    private GenreDTO genre;
}
