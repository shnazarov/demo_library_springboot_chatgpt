package com.epam.bookdemo.service;

import com.epam.bookdemo.dto.BookDTO;

public interface BookService extends CrudService<BookDTO, Long> {
}
