package com.epam.bookdemo.service;

import com.epam.bookdemo.dto.GenreDTO;

import java.util.List;

public interface GenreService extends CrudService<GenreDTO, Long> {


}
