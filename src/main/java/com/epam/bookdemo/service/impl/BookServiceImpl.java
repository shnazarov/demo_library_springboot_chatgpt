package com.epam.bookdemo.service.impl;

import com.epam.bookdemo.dto.BookDTO;
import com.epam.bookdemo.exception.DataNotFoundException;
import com.epam.bookdemo.mapper.BookMapper;
import com.epam.bookdemo.repository.BookRepository;
import com.epam.bookdemo.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private static final BookMapper bookMapper = BookMapper.INSTANCE;
    private final BookRepository bookRepository;


    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookDTO create(BookDTO dto) {
        var book = bookMapper.toModel(dto);
        var savedBook = bookRepository.save(book);

        return bookMapper.toDTO(savedBook);
    }

    @Override
    public BookDTO update(BookDTO dto) {
        var book = bookMapper.toModel(dto);
        bookRepository.findById(book.getId())
                .orElseThrow(() -> new DataNotFoundException("Book", "id", book.getId()));

        var updatedBook = bookRepository.save(book);
        return bookMapper.toDTO(updatedBook);
    }

    @Override
    public BookDTO getById(Long id) {
        var book = bookRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Book", "id", id));

        return bookMapper.toDTO(book);
    }

    @Override
    public List<BookDTO> getAll() {
        var books = bookRepository.findAll();
        return bookMapper.toDTOs(books);
    }

    @Override
    public void delete(Long id) {
        var book = bookRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Book", "id", id));

        bookRepository.delete(book);
    }
}
