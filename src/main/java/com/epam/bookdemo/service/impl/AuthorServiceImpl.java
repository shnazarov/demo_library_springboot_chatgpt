package com.epam.bookdemo.service.impl;

import com.epam.bookdemo.dto.AuthorDTO;
import com.epam.bookdemo.exception.DataNotFoundException;
import com.epam.bookdemo.mapper.AuthorMapper;
import com.epam.bookdemo.repository.AuthorRepository;
import com.epam.bookdemo.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {

    private static final AuthorMapper authorMapper = AuthorMapper.INSTANCE;
    private final AuthorRepository authorRepository;


    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public AuthorDTO create(AuthorDTO dto) {
        var author = authorMapper.toModel(dto);
        var savedAuthor = authorRepository.save(author);
        return authorMapper.toDTO(savedAuthor);
    }

    @Override
    public AuthorDTO update(AuthorDTO dto) {
        var author = authorMapper.toModel(dto);
        var updatedAuthor = authorRepository.save(author);
        return authorMapper.toDTO(updatedAuthor);
    }

    @Override
    public AuthorDTO getById(Long id) {
        var author = authorRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Author", "id", id));

        return authorMapper.toDTO(author);
    }

    @Override
    public List<AuthorDTO> getAll() {
        var authors = authorRepository.findAll();
        return authorMapper.toDTOs(authors);
    }

    @Override
    public void delete(Long id) {
        var author = authorRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Author", "id", id));

        authorRepository.delete(author);
    }
}
