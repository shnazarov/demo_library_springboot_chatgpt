package com.epam.bookdemo.service.impl;

import com.epam.bookdemo.dto.GenreDTO;
import com.epam.bookdemo.exception.DataNotFoundException;
import com.epam.bookdemo.mapper.GenreMapper;
import com.epam.bookdemo.repository.GenreRepository;
import com.epam.bookdemo.service.GenreService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {
    private static final GenreMapper genreMapper = GenreMapper.INSTANCE;

    private final GenreRepository genreRepository;

    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public GenreDTO create(GenreDTO dto) {
        var genre = genreMapper.toModel(dto);
        var savedGenre = genreRepository.save(genre);
        return genreMapper.toDTO(savedGenre);
    }

    @Override
    public GenreDTO update(GenreDTO dto) {
        var genre = genreMapper.toModel(dto);
        var updatedGenre = genreRepository.save(genre);
        return genreMapper.toDTO(updatedGenre);
    }

    @Override
    public GenreDTO getById(Long id) {
        var genre = genreRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Genre", "id", id));
        return genreMapper.toDTO(genre);
    }

    @Override
    public List<GenreDTO> getAll() {
        var genres = genreRepository.findAll();
        return genreMapper.toDTOs(genres);
    }

    @Override
    public void delete(Long id) {
        var genre = genreRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Genre", "id", id));

        genreRepository.delete(genre);
    }
}
