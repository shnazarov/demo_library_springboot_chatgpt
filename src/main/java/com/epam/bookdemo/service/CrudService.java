package com.epam.bookdemo.service;

import java.util.List;

public interface CrudService<T, ID> {

    T create(T dto);

    T update(T dto);

    T getById(ID id);

    List<T> getAll();

    void delete(ID id);
}
