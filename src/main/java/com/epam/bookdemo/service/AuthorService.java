package com.epam.bookdemo.service;

import com.epam.bookdemo.dto.AuthorDTO;

public interface AuthorService extends CrudService<AuthorDTO, Long> {
}
