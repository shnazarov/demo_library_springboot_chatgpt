package com.epam.bookdemo.controller;

import com.epam.bookdemo.dto.ApiResponse;
import com.epam.bookdemo.dto.AuthorDTO;
import com.epam.bookdemo.service.AuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/author")
public class AuthorController {

    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/")
    public ResponseEntity<List<AuthorDTO>> getAllAuthors() {
        var authors = authorService.getAll();
        return ResponseEntity.ok(authors);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDTO> getAuthorById(@PathVariable("id") Long id) {
        var author = authorService.getById(id);
        return ResponseEntity.ok(author);
    }

    @PostMapping("/")
    public ResponseEntity<AuthorDTO> createAuthor(@RequestBody AuthorDTO authorDTO) {
        var author = authorService.create(authorDTO);
        return ResponseEntity.ok(author);
    }

    @PutMapping("/")
    public ResponseEntity<AuthorDTO> updateAuthor(@RequestBody AuthorDTO authorDTO) {
        var updatedAuthor = authorService.update(authorDTO);
        return ResponseEntity.ok(updatedAuthor);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable("id") Long id) {
        var result = ApiResponse.builder()
                .message("author deleted successfully")
                .success(true)
                .build();

        return ResponseEntity.ok(result);
    }
}
