package com.epam.bookdemo.controller;

import com.epam.bookdemo.dto.ApiResponse;
import com.epam.bookdemo.dto.BookDTO;
import com.epam.bookdemo.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/book")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/")
    public ResponseEntity<List<BookDTO>> getAllBooks() {
        var books = bookService.getAll();
        return ResponseEntity.ok(books);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDTO> getBookById(@PathVariable("id") Long id) {
        var book = bookService.getById(id);

        return ResponseEntity.ok(book);
    }

    @PostMapping("/")
    public ResponseEntity<BookDTO> createBook(@RequestBody BookDTO bookDTO) {
        var book = bookService.create(bookDTO);

        return ResponseEntity.ok(book);
    }

    @PutMapping("/")
    public ResponseEntity<BookDTO> updateBook(@RequestBody BookDTO bookDTO) {
        var updatedBook = bookService.update(bookDTO);

        return ResponseEntity.ok(updatedBook);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteBook(@PathVariable("id") Long id) {
        bookService.delete(id);
        var result = ApiResponse.builder()
                .message("Book deleted successfully")
                .success(true)
                .build();
        return ResponseEntity.ok(result);
    }

}
