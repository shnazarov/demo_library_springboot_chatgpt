package com.epam.bookdemo.controller;

import com.epam.bookdemo.dto.ApiResponse;
import com.epam.bookdemo.dto.GenreDTO;
import com.epam.bookdemo.service.GenreService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/genre")
public class GenreController {

    private final GenreService genreService;

    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping("/")
    public ResponseEntity<List<GenreDTO>> getAllGenres() {
        var genres = genreService.getAll();
        return ResponseEntity.ok(genres);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GenreDTO> getGenreById(@PathVariable("id") Long id) {
        var genre = genreService.getById(id);
        return ResponseEntity.ok(genre);
    }

    @PostMapping("/")
    public ResponseEntity<GenreDTO> createGenre(@RequestBody GenreDTO genreDTO) {
        var genre = genreService.create(genreDTO);
        return ResponseEntity.ok(genre);
    }

    @PutMapping("/")
    public ResponseEntity<GenreDTO> updateGenre(@RequestBody GenreDTO genreDTO) {
        var updatedGenre = genreService.update(genreDTO);
        return ResponseEntity.ok(updatedGenre);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable("id") Long id) {
        var result = ApiResponse.builder()
                .message("genre deleted successfully")
                .success(true)
                .build();

        return ResponseEntity.ok(result);
    }
}
